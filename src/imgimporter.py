import re
import os
import shutil
import threading
import queue
from exif import Image


class MissingExifData(Exception):
    pass


class NoData(Exception):
    pass


def get_date(fname):
    result = None
    with open(fname, 'rb') as image_file:
        my_image = Image(image_file)
        try:
            ts = my_image.datetime_original
            ts = re.split('[: ]', ts)[:3]
            result = (ts[0], ts[1], ts[2])
        except KeyError:
            raise MissingExifData(f'Missing datetime_original exif data in {fname}')
    return result


def find_files(directory):
    output = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            file = os.path.join(root, file)
            filename, file_extension = os.path.splitext(file)
            if file_extension.lower() in ['.jpg', '.jpeg', '.raw', '.raf']:
                output.append(file)
    return output


def move_file(source, target_dir, dry_run, overwrite, remove_source):
    yyyy, mm, dd = get_date(source)
    full_target_dir = os.path.join(f'{target_dir}/{yyyy}', f'{yyyy}-{mm}-{dd}')
    file = os.path.basename(source)
    full_target_file = os.path.join(full_target_dir, file)
    if os.path.exists(full_target_file) and not overwrite:
        raise FileExistsError(full_target_file)
    else:
        if not dry_run:
            os.makedirs(full_target_dir, exist_ok=True)
            shutil.copy(source, full_target_dir)
            shutil.copystat(source, full_target_file)
            if remove_source:
                os.unlink(source)
    return True


class ImporterThread:
    def __init__(self, source, target, dry_run, overwrite, remove):
        self.thread_queue = queue.Queue()
        self.abort_thread = threading.Event()
        args = dict(source=source, target=target, dry_run=dry_run, overwrite=overwrite, remove=remove)
        self.worker_thread = threading.Thread(target=self.importer, kwargs=args)
        self.worker_thread.start()

    def abort(self):
        self.abort_thread.set()

    def join(self):
        self.worker_thread.join()

    def running(self):
        return self.worker_thread.is_alive() or self.thread_queue.qsize() > 0

    def receive(self):
        try:
            return self.thread_queue.get(False)
        except queue.Empty:
            raise NoData()

    def importer(self, source, target, dry_run, overwrite, remove):
        for file in find_files(source):
            try:
                if self.abort_thread.is_set():
                    break
                move_file(file, target, dry_run, overwrite, remove)
                self.thread_queue.put((file, 'OK', ''))
            except MissingExifData:
                self.thread_queue.put((file, 'FAIL', 'File is missing required exif data'))
            except FileExistsError:
                self.thread_queue.put((file, 'FAIL', 'File already exists'))
            except OSError as e:
                self.thread_queue.put((file, 'FAIL', f'{e.strerror}'))
