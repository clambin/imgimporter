import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox
from tkinter import ttk
import PIL
import PIL.ImageTk
import PIL.ImageOps
import exif
import rawpy
import io


class FileChooser(ttk.Frame):
    def __init__(self, parent, label, callback=None, **kwargs):
        super().__init__(parent, **kwargs)
        self.callback = callback

        self.selected_value = tk.StringVar()

        self.label = ttk.Label(self, text=label)
        self.label.grid(column=0, row=0)

        self.selected_entry = ttk.Entry(self, textvariable=self.selected_value, width=60, state=tk.DISABLED)
        self.selected_entry.grid(column=1, row=0)

        self.select = ttk.Button(self, text='Select', command=self.select_cb)
        self.select.grid(column=2, row=0)

    def select_cb(self):
        current = self.selected_value.get()
        file = tkinter.filedialog.askopenfilename(parent=self, title='Select image', initialfile=current)
        if file:
            self.selected_value.set(file)
            if self.callback:
                self.callback(file)


class PhotoViewer(tk.Canvas):
    def __init__(self, parent, **kwargs):
        super().__init__(parent, **kwargs)
        self.image = None
        self.canvas_image = None
        self.image_id = None
        self.width = self.winfo_reqwidth()
        self.height = self.winfo_reqheight()
        self.angle = 0
        self.bind('<Configure>', self.on_resize)

    def process_image(self):
        image = self.image
        if self.angle:
            image = image.rotate(self.angle, expand=True)
        ratio = image.width / image.height
        width, height = self.width, self.height
        # resize the image.respect the image's aspect ratio, but don't truncate part of the image
        if width < ratio * height:
            height = int(width / ratio)
        elif height < width / ratio:
            width = int(height * ratio)
        image = image.resize((width, height), PIL.Image.ANTIALIAS)
        return PIL.ImageTk.PhotoImage(image)

    def show_image(self):
        if self.image:
            self.canvas_image = self.process_image()
            self.image_id = self.create_image(0, 0, image=self.canvas_image, anchor='nw')
            self.itemconfigure(self.image_id, image=self.canvas_image)

    def on_resize(self, event):
        self.width = event.width
        self.height = event.height
        if self.image_id:
            self.show_image()

    @staticmethod
    def _load_image(fname):
        output = None
        try:
            output = PIL.Image.open(fname)
        except PIL.UnidentifiedImageError:
            try:
                with rawpy.imread(fname) as content:
                    # fixme: extract_thumb may raise LibRawNoThumbnailError or LibRawUnsupportedThumbnailError
                    thumb = content.extract_thumb()
                    buffer = io.BytesIO(thumb.data)
                    if thumb.format == rawpy.ThumbFormat.JPEG:   # noqa
                        output = PIL.Image.open(buffer)
            except rawpy.LibRawFileUnsupportedError:  # noqa
                pass
        return output

    @staticmethod
    def _read_angle(image):
        try:
            orientation = image._getexif()[0x0112]  # noqa
            if orientation == exif.Orientation.LEFT_BOTTOM:
                return 90
            # TODO: check for each orientation
            elif orientation == exif.Orientation.TOP_RIGHT:
                return -90
            elif orientation == exif.Orientation.RIGHT_BOTTOM:
                return 180
            return 0
        except AttributeError:
            return 0

    def load(self, fname):
        self.image = self._load_image(fname)
        if self.image:
            self.angle = self._read_angle(self.image)
            self.show_image()

    def rotate_image(self, angle):
        self.angle += angle
        self.show_image()


class Commands(ttk.Frame):
    def __init__(self, parent, app, **kwargs):
        super().__init__(parent, **kwargs)

        self.chooser = FileChooser(self, 'Image:', app.load_image)
        self.chooser.grid(column=0, row=0)

        rotate_icon = PIL.Image.open('../resources/rotate-16.gif')
        self.icon_left = PIL.ImageTk.PhotoImage(rotate_icon)
        self.rotate_left = ttk.Button(self, image=self.icon_left, command=app.rotate_left)
        self.rotate_left.grid(column=1, row=0)

        rotate_icon = PIL.ImageOps.mirror(rotate_icon)
        self.icon_right = PIL.ImageTk.PhotoImage(rotate_icon)
        self.rotate_right = ttk.Button(self, image=self.icon_right, command=app.rotate_right)
        self.rotate_right.grid(column=2, row=0)


class UI:
    def __init__(self, parent):
        self.parent = parent
        self._init_gui()

    def _init_gui(self):
        self.parent.title('Image Viewer')

        self.commands = Commands(self.parent, self)
        self.commands.grid(column=0, row=0, sticky='ew')

        self.viewer = PhotoViewer(self.parent, width=500, height=500, background='lightgrey')
        self.viewer.grid(column=0, row=2, columnspan=3, sticky='nesw')

        self.parent.columnconfigure(0, weight=1)
        self.parent.rowconfigure(2, weight=1)

    def load_image(self, filename):
        self.viewer.load(filename)
        self.commands.rotate_left['state'] = tk.NORMAL
        self.commands.rotate_right['state'] = tk.NORMAL

    def rotate_left(self):
        self.viewer.rotate_image(90)

    def rotate_right(self):
        self.viewer.rotate_image(-90)


def main():
    master = tk.Tk()
    ui = UI(master)     # noqa: F841
    master.mainloop()


if __name__ == '__main__':
    main()
