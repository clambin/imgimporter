import time
import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox
from tkinter import ttk

from src import imgimporter


class CommandFrame(ttk.Frame):
    def __init__(self, parent, app):
        super().__init__(parent, relief=tk.GROOVE, padding='0.2i')
        self.parent = parent
        self.app = app

        self.source_label = ttk.Label(self, text='Source:')
        self.source_label.grid(column=0, row=0)

        self.source_value = tk.StringVar()
        self.source_value.set(self.app.default_source_dir)
        self.source_entry = ttk.Entry(self, textvariable=self.source_value, width=40, state=tk.DISABLED)
        self.source_entry.grid(column=1, row=0)

        self.source_select = ttk.Button(self, text='Select', command=self.source_cb)
        self.source_select.grid(column=2, row=0)

        self.target_target = ttk.Label(self, text='Target:')
        self.target_target.grid(column=0, row=1)

        self.target_value = tk.StringVar()
        self.target_value.set(self.app.default_target_dir)
        self.target_entry = ttk.Entry(self, textvariable=self.target_value, width=40, state=tk.DISABLED)
        self.target_entry.grid(column=1, row=1)

        self.target_select = ttk.Button(self, text='Select', command=self.target_cb)
        self.target_select.grid(column=2, row=1)

        self.import_button = ttk.Button(self, text='Import', command=self.import_cb)
        self.import_button.grid(column=3, row=0)

        self.abort_button = ttk.Button(self, text='Abort', command=self.abort_cb, state=tk.DISABLED)
        self.abort_button.grid(column=3, row=1)

        self.dry_run_value = tk.IntVar()
        self.dry_run_value.set(1)
        self.dry_run_cbt = ttk.Checkbutton(self, text='Dry run', variable=self.dry_run_value)
        self.dry_run_cbt.grid(column=4, row=0, sticky='w')

        self.overwrite_value = tk.IntVar()
        self.overwrite_value.set(0)
        self.overwrite_cbt = ttk.Checkbutton(self, text='Overwrite existing files', variable=self.overwrite_value)
        self.overwrite_cbt.grid(column=4, row=1, sticky='w')

        self.remove_value = tk.IntVar()
        self.remove_value.set(0)
        self.remove_cbt = ttk.Checkbutton(self, text='Remove source files after succesful import',
                                          variable=self.remove_value)
        self.remove_cbt.grid(column=4, row=2, sticky='w')

    def source_cb(self):
        current = self.source_value.get()
        directory = tkinter.filedialog.askdirectory(parent=None, title='Source directory', initialdir=current)
        if directory:
            self.source_value.set(directory)

    def target_cb(self):
        current = self.target_value.get()
        directory = tkinter.filedialog.askdirectory(parent=None, title='Target directory', initialdir=current)
        if directory:
            self.target_value.set(directory)

    def set_gui(self, state):
        self.source_select['state'] = state
        self.target_select['state'] = state
        self.import_button['state'] = state
        self.abort_button['state'] = tk.DISABLED if state == tk.ACTIVE else tk.ACTIVE
        self.dry_run_cbt['state'] = state
        self.overwrite_cbt['state'] = state
        self.remove_cbt['state'] = state

    def import_cb(self):
        self.app.start_import()

    def abort_cb(self):
        self.app.abort_import()


class ProgressReport(ttk.Frame):
    def __init__(self, parent, height=25):
        super().__init__(parent, relief=tk.GROOVE)
        self.parent = parent

        self.tree = ttk.Treeview(self, columns=('source', 'result', 'errmsg'), selectmode='none', height=height)
        self.ysb = ttk.Scrollbar(self, orient=tk.VERTICAL, command=self.tree.yview)
        self.xsb = ttk.Scrollbar(self, orient=tk.HORIZONTAL, command=self.tree.xview)
        self.tree.configure(yscrollcommand=self.ysb.set)
        self.tree.configure(xscrollcommand=self.xsb.set)

        self.tree.grid(row=0, column=0, sticky='nsew')
        self.ysb.grid(row=0, column=1, sticky='ns')
        self.xsb.grid(row=1, column=0, sticky='ew')

        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.tree.column('#0', width=0, minwidth=0, stretch=tk.NO)
        self.tree.column('source', width=500, minwidth=300)
        self.tree.column('result', width=50, minwidth=50, anchor="center")
        self.tree.column('errmsg', width=200, minwidth=200)
        self.tree.heading('source', text='Source file', anchor='w')  # TODO: anchor is ignored
        self.tree.heading('result', text='Result', anchor='w')
        self.tree.heading('errmsg', text='Error message', anchor='w')

        self.tree.tag_configure('monospace', font='courier 12')

    def reset(self):
        for iid in self.tree.get_children():
            self.tree.delete(iid)

    def add_row(self, data):
        self.tree.insert(values=data, index='end', parent='', tag='monospace')
        self.tree.yview_moveto(1)


class SDImporterUI:
    default_source_dir = '/Users/christophe/Desktop/saved'
    default_target_dir = '/Users/christophe/Desktop/copy'

    def __init__(self, parent):
        self.parent = parent
        self.importer = None
        self.progress = tk.DoubleVar()
        self.nr_of_files = None
        self.imported = None
        self.start_time = None
        self._init_gui()

    def _init_gui(self):
        self.parent.title('SD Importer')
        self.parent.resizable(False, False)

        self.cmds = CommandFrame(self.parent, self)
        self.cmds.grid(column=0, row=0)

        self.progressreport = ProgressReport(self.parent)
        self.progressreport.grid(column=0, row=1, sticky='nesw')

        self.separator = ttk.Separator(self.parent, orient=tk.HORIZONTAL)
        self.separator.grid(column=0, row=2, sticky='we')

        self.progressbar = ttk.Progressbar(self.parent, orient=tk.HORIZONTAL, mode='determinate',
                                           variable=self.progress)
        self.progressbar.grid(column=0, row=3, sticky='we')

    def start_import(self):
        self.nr_of_files = len(imgimporter.find_files(self.cmds.source_value.get()))
        if self.nr_of_files == 0:
            tkinter.messagebox.showwarning('', 'No importable files found')
            return
        self.cmds.set_gui(tk.DISABLED)
        self.progressreport.reset()
        self.progress.set(0.0)
        self.imported = 0
        self.start_time = time.time()
        self.importer = imgimporter.ImporterThread(
            self.cmds.source_value.get(),
            self.cmds.target_value.get(),
            self.cmds.dry_run_value.get(),
            self.cmds.overwrite_value.get(),
            self.cmds.remove_value.get()
        )
        self.parent.after(100, self.listen_for_results)

    def process_import(self):
        try:
            while True:
                (file, result, errmsg) = self.importer.receive()
                self.progressreport.add_row((file, result, errmsg))
                self.progress.set(self.progress.get() + (100 / self.nr_of_files))
                if result == 'OK':
                    self.imported += 1
        except imgimporter.NoData:
            pass

    def end_import(self):
        self.importer.join()
        if self.imported == self.nr_of_files:
            diff = time.time() - self.start_time
            tkinter.messagebox.showinfo('Success',
                                        f'Successfully imported {self.imported} files in {diff:.1f} seconds')
        elif self.imported == 0:
            tkinter.messagebox.showwarning('Warning', 'No files imported')
        else:
            tkinter.messagebox.showwarning('Warning',
                                           f'{self.nr_of_files - self.imported} files could not be imported')
        self.cmds.set_gui(tk.ACTIVE)

    def abort_import(self):
        self.importer.abort()

    def listen_for_results(self):
        self.process_import()
        if self.importer.running():
            self.parent.after(100, self.listen_for_results)
        else:
            self.end_import()


def main():
    master = tk.Tk()
    ui = SDImporterUI(master)   # noqa: F841
    master.mainloop()
