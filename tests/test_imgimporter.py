import pytest
import shutil
import os
import time
from src import imgimporter


def test_get_date():
    result = imgimporter.get_date('./images/test_exif.jpg')
    assert result == ('2006', '04', '06')
    with pytest.raises(imgimporter.MissingExifData):
        imgimporter.get_date('./images/test_noexif.jpg')
    with pytest.raises(imgimporter.MissingExifData):
        imgimporter.get_date('./images/test_nopic.txt')
    with pytest.raises(FileNotFoundError):
        imgimporter.get_date('./images/notafile.jpg')


def test_find_files():
    result = imgimporter.find_files('./images/')
    assert sorted(result) == [
        './images/test_exif.jpg',
        './images/test_noexif.jpg'
    ]
    result = imgimporter.find_files('./notadirectory')
    assert result == []


def wipe_dir(directory):
    try:
        if os.path.isdir(directory):
            shutil.rmtree(directory)
    except FileNotFoundError:
        pass


def test_move_file():
    wipe_dir('./output')
    assert imgimporter.move_file('./images/test_exif.jpg', './output', False, False, False) is True
    with pytest.raises(FileExistsError):
        assert imgimporter.move_file('./images/test_exif.jpg', './output', False, False, False) is True
    assert imgimporter.move_file('./images/test_exif.jpg', './output', False, True, False) is True
    with pytest.raises(FileNotFoundError):
        assert imgimporter.move_file('./images/test_notafile.jpg', './output', False, False, False) is True
    with pytest.raises(imgimporter.MissingExifData):
        assert imgimporter.move_file('./images/test_noexif.jpg', './output', False, False, False) is True
    shutil.copyfile('./images/test_exif.jpg', './images/copy.jpg')
    assert imgimporter.move_file('./images/copy.jpg', './output', False, False, True) is True
    assert os.path.isfile('./output/2006/2006-04-06/copy.jpg') is True
    assert os.path.isfile('./images/copy.jpg') is False
    wipe_dir('./output')


def test_importer_thread():
    wipe_dir('./output')
    importer = imgimporter.ImporterThread('./images', './output', False, False, False)
    importer.join()
    result = []
    try:
        while True:
            result.append(importer.receive())
    except imgimporter.NoData:
        pass
    assert len(result) == 2
    assert result[0] == ('./images/test_exif.jpg', 'OK', '')
    assert result[1] == ('./images/test_noexif.jpg', 'FAIL', 'File is missing required exif data')
    wipe_dir('./output')


def test_slow_importer():
    wipe_dir('./output')
    importer = imgimporter.ImporterThread('./images', './output', False, False, False)
    while importer.running():
        try:
            importer.receive()
        except imgimporter.NoData:
            pass
        time.sleep(1)
    importer.join()
    assert importer.thread_queue.qsize() == 0
    wipe_dir('./output')


def test_importer_no_overwrite():
    wipe_dir('./output')
    importer = imgimporter.ImporterThread('./images', './output', False, False, False)
    importer.join()
    importer = imgimporter.ImporterThread('./images', './output', False, False, False)
    importer.join()
    result = []
    try:
        while True:
            result.append(importer.receive())
    except imgimporter.NoData:
        pass
    assert len(result) == 2
    assert result[0] == ('./images/test_exif.jpg', 'FAIL', 'File already exists')
    assert result[1] == ('./images/test_noexif.jpg', 'FAIL', 'File is missing required exif data')
    wipe_dir('./output')
